任务队列
	普通队列:
		BRPOP如果队列qe没用任务就会一直阻塞等待
		1>BRPOP qe 0
		2>LPUSH qe qe
		场景:
		  	用户初登录一个网站，需要注册等待邮件通知确认，网站的邮件发送任务都是用队列来排序执行的，另外还有其他任务邮件在排队
			所以要优先处理发送确认的邮件
			BLPOP qe:confirm.email,qe:other.email
			(这里的优先级是从左到右)
	发布订阅：
		订阅->SUBCRIBE channel
		发布->PUBLISH channel
		tip:
			先订阅了再发布
		按照规则订阅:
			PSUBCRIBE channel.?*
			匹配所有channel.开头的队列
