 Redis事务
	MULT->开启事务，exec->结束事务
	tip:
		事务中的命令都是执行exec命令后一起执行的，在事务中敲下的命令只是进入一个命令队列，并没有执行
	qe:
		如果事务中某一个命令的参数是下一个命令的结果，这样是没办法达到预期的
	resolve:
		使用watch
	watch/unwatch:
		watch监视一个key,如果事务执行之前key被修改，那么事务就无法进行，watch一直工作到exec命令
		解决场景:
			watch key
				v = get key
				v = v + 1
			MULT
			set key,v
			EXEC
			这里先把key的副本赋给另一个临时变量，同时监控key，如果在执行事务之前key就被其余修改，那么事务不会执行(防止竟态)
	过期/频率访问：
		命令：
			EXPIRE key,time(秒)->设置key的过期时间，TTL key->查看过期时间, PERSIST key->取消过期时间
		tip:
			设置了过期时间之后，到达时间就会删除该键
		场景：
			描述：
				网站现在需要限制一个用户每个时间段内的最大访问量，比如1分钟内最多访问10次(可能有人使用脚本大批量访问攻击)
			resolve:
				if (EXISTS rate.limiting:userIp) {
					times = INCR rate.limiting:userIp;
					if(times > 10) {
						print:访问超过限制;
					}
				} ELSE {
					MULT
						INCR rate.limiting:userIp;
						EXPIRE key,60
					EXEC
				}
			延伸：
				  如果一个用户在这一分钟的第一秒访问了一次，然后在同一分钟的最后一秒时刻访问了九次，然后又在下一分钟的第一秒访问19次，
				这样就造成了两秒内访问了19次的意外；
			resolve:
				  使用一个队列，把每次访问的时间记录下下来，一旦记录超过十个，就判断时间最早的那个元素和现在的元素之间差值是否小于1分钟；
				是则就限流，不是则就将现在的时间加入队列把最早的元素删了；
				int l = LLEN rate.limiting:userIp;
				if (l < 10) {
					// 没到一分钟
					LPUSH rate.limiting:userIp, now();
				} else {
					// 一分钟时间到
					// 初始时间
					date time = LINDEX rate.limiting:userIp,-1;
					// 一分钟不到已经访问超过十次
					if (now() - time < 60) {
						print: 限流;
					} else {
						LPUSH LLEN rate.limiting,now;
						LTRIM LLEN rate.limiting,-1;
					}
				}


